<%--
  Created by IntelliJ IDEA.
  User: 跨越时空的记忆
  Date: 2021/4/29
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <meta charset="UTF-8">
    <title>登陆界面</title>
    <script type="text/javascript" src="js/jquery-1.11.1.js" ></script>
    <script type="text/javascript" src="js/bootstrap.js" ></script>
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/bootstrap-theme.css" />
  </head>
  <body style="background-color:rgb(222,222,222);">

  <%
    Cookie[] cookies = request.getCookies();
    String value = "";
    if(cookies!=null && cookies.length>0){
      for (Cookie cookie : cookies) {
        if(cookie.getName().equals("uname")){
          value = cookie.getValue();
          break;
        }
      }
    }
    pageContext.setAttribute("unamecookie",value);
  %>

  <div class="container">
    <form action="logon" method="post" role="form" class="form-horizontal">
      <div class="row">
        <div class="col-sm-10">
          &emsp;<h3>登录页面</h3><br/>
        </div>
      </div>
      <div class="form-group">
        <label for="name" class="col-sm-1 control-label">用户名：</label>
        <div class="col-sm-10">
          <input type="text" id="name" name="userName" placeholder="请输入您的用户名" required="required" value="${unamecookie}"/>
        </div>
      </div>
      <div class="form-group">
        <label for="pwd" class="col-sm-1 control-label">密码：</label>
        <div class="col-sm-10">
          <input type="password" id="pwd" name="pwd" placeholder="请输入您的密码" required="required"/>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-1 col-sm-10">
          <input type="submit" value="登录" class="btn btn-primary"/>
          <input type="reset" value="清空" class="btn btn-default"/>
        </div>
      </div>
    </form>
  </div>
  </body>
</html>
