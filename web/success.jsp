<%--
  Created by IntelliJ IDEA.
  User: 跨越时空的记忆
  Date: 2021/5/4
  Time: 11:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登陆成功页面</title>
    <script type="text/javascript" src="js/jquery-1.11.1.js" ></script>
    <script type="text/javascript" src="js/bootstrap.js" ></script>
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/bootstrap-theme.css" />
</head>
<body>
    <%--<%
        String userName = (String) request.getAttribute("userName");
        session.setAttribute("userName",userName);
    %>--%>
    <h3>欢迎您，${sessionScope.userName} </h3>
    <a id="btn" class="btn btn-default" href="loginout">退出</a>

</body>
</html>
<%--<script>
    $(function(){
        $("a[id='btn']").click(function(){
            <% session.invalidate(); %>
        });
    });
</script>--%>
