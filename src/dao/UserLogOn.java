package dao;

import bean.Users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserLogOn extends BaseDao {

    public Users getUsersByUserName(String userName) {
        Users users = new Users();
        try {
            String sql = "select * from users where userName = ?";
            List list = new ArrayList();
            list.add(userName);
            ResultSet rs = query(sql, list);
            while (rs.next()){
                users.setPassward(rs.getString("passward"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            closeall();
        }
        return users;
    }
}
