package util;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FirstFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("初始化过滤器");
    }

    @Override  //请求和响应时都会执行这个方法
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("过滤器开始");
        //处理乱码
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;
        request.setCharacterEncoding("utf-8");

        //防止用户在未登陆的情况下访问资源
        String requestURI = request.getRequestURI();
        System.out.println("requestURI="+requestURI);
        Object username = request.getSession().getAttribute("userName");

        if(requestURI.endsWith("success.jsp")&&username==null){
            response.sendRedirect("index.jsp");
        }

        //调取下一个过滤器，或者调取servlet
        filterChain.doFilter(servletRequest, servletResponse);
        System.out.println("过滤器结束");
    }

    @Override
    public void destroy() {
        System.out.println("销毁过滤器");
    }
}
