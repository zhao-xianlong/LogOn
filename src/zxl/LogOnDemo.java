package zxl;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

import bean.Users;
import dao.UserLogOn;

public class LogOnDemo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("登录被执行");
        String userName = req.getParameter("userName");
        String pwd = req.getParameter("pwd");

        /*//将用户名存放在request中
        req.setAttribute("userName",userName);*/

        UserLogOn userLogOn = new UserLogOn();
        Users user = userLogOn.getUsersByUserName(userName);
        String passward = user.getPassward();
        if(pwd.equals(passward)){
            //将用户名和密码存放在session中
            HttpSession session = req.getSession();
            session.setAttribute("userName",userName);
            session.setAttribute("password",pwd);
            /*req.getRequestDispatcher("/success.jsp").forward(req,resp);*/
            resp.sendRedirect("success.jsp");
        }else{
            //1.后台创建coolkie
            Cookie cookie = new Cookie("uname",userName);
            //2.返回给前端
            resp.addCookie(cookie);
            //req.getRequestDispatcher("/defeat.html").forward(req,resp);
            resp.sendRedirect("index.jsp");
        }

    }
}
